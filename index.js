const bodyParser = require('body-parser');
const express = require('express');
const dbConnect = require('./config/dbConnect');
const authRoute = require('./routes/authRoute');

const app = express();
const dotenv = require('dotenv').config();

dbConnect();
const port = process.env.PORT || 4000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/api/auth", authRoute);

app.listen(port, () => console.log(`listening on http://localhost:${port}`));
