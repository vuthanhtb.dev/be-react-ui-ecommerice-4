const mongoose = require('mongoose');
mongoose.set('strictQuery', true);

const dbConnect = () => {
  try {
    mongoose.connect(process.env.MONGODB_URL);
    console.log("database connected successfully");
  } catch (error) {
    console.log(error);
  }
};

module.exports = dbConnect;
