const express = require('express');
const { createUser } = require('../controller/userController');

const authRoute = express.Router();

authRoute.post("/register", createUser);

module.exports = authRoute;
