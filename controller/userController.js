const User = require("../model/user");

const createUser = async (req, res) => {
  const email = req.body.email;
  const foundUser = await User.findOne({email: email});


  if (!foundUser) {
    const newUser = await User.create(req.body);
  console.log(newUser)

    res.json(newUser);
  } else {
    res.json({
      message: "User already exists",
      success: false
    });
  }
};

module.exports = {
  createUser
};
